package com.nqm.graduation.file.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileInfoResponse {
    private Long id;

    private String fileName;

    private String originalFilename;

    private String url;

    private String thumbUrl;

    private String mimeType;

    private Long fileSize;

}
