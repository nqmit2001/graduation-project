package com.nqm.graduation.file.repository;

import com.nqm.graduation.file.entity.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileStorageRepository extends JpaRepository<FileInfo, Long> {
}
