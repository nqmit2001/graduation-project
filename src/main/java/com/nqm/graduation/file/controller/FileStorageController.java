package com.nqm.graduation.file.controller;

import com.nqm.graduation.file.dto.FileInfoResponse;
import com.nqm.graduation.file.entity.FileInfo;
import com.nqm.graduation.file.service.FileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("file")
@Api(tags = "File", value = "File", description = "API File")
public class FileStorageController {
    private final FileStorageService filesStorageService;

    public FileStorageController(FileStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Operation(
            description = "Tìm kiếm file theo id")
    @ApiResponses(
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content(schema = @Schema(implementation = String.class, example = "example")))
    )
    @ApiOperation(value = "Tìm kiếm file theo id", response = Object.class)
    @GetMapping("{id}")
    public ResponseEntity<?> findById(@RequestParam("id") Long id) {
        FileInfoResponse response = filesStorageService.findById(id);
        return ResponseEntity.ok(response);
    }

    @Operation(
            description = "Tìm kiếm file theo danh sách id")
    @ApiResponses(
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content(schema = @Schema(implementation = String.class, example = "example")))
    )
    @ApiOperation(value = "Tìm kiếm file theo danh sách id", response = Object.class)
    @GetMapping("{ids}")
    public ResponseEntity<?> findByIds(@RequestParam("ids") List<Long> ids) {
        List<FileInfoResponse> response = filesStorageService.findByIds(ids);
        return ResponseEntity.ok(response);
    }

    @Operation(
            description = "Tải file mới")
    @ApiResponses(
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content(schema = @Schema(implementation = String.class, example = "example")))
    )
    @ApiOperation(value = "Tải file mới", response = Object.class)
    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String uploadUrl = filesStorageService.uploadFile(file);
        return ResponseEntity.ok(uploadUrl);
    }

    @Operation(
            description = "Tải nhiều file mới")
    @ApiResponses(
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content(schema = @Schema(implementation = List.class, example = "example")))
    )
    @ApiOperation(value = "Tải nhiều mới", response = Object.class)
    @PostMapping("upload-multiple")
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        List<String> uploadUrls = filesStorageService.uploadMultipleFiles(files);
        return ResponseEntity.ok(uploadUrls);
    }
}
