package com.nqm.graduation.file.mapper;

import com.nqm.graduation.file.dto.FileInfoResponse;
import com.nqm.graduation.file.entity.FileInfo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileInforMapper {

    FileInforMapper MAPPER = Mappers.getMapper(FileInforMapper.class);

    FileInfoResponse toResponse(FileInfo fileInfo);

    List<FileInfoResponse> toResponse(List<FileInfo> fileInfos);
}
