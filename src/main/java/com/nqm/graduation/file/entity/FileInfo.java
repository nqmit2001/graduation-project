package com.nqm.graduation.file.entity;

import com.nqm.graduation.common.auditting.AuditableEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "file")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileInfo extends AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @Column(name = "file_name")
    private String fileName;

    @Column(name = "original_file_name")
    private String originalFilename;
    @Column(name = "original_url")
    private String url;

    @Column(name = "thumb_url")
    private String thumbUrl;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "file_size")
    private Long fileSize;
}
