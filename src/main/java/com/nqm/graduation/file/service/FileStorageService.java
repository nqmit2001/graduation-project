package com.nqm.graduation.file.service;

import com.nqm.graduation.file.dto.FileInfoResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileStorageService {
    FileInfoResponse findById(Long id);

    List<FileInfoResponse> findByIds(List<Long> ids);

    String uploadFile(MultipartFile file);

    List<String> uploadMultipleFiles(MultipartFile[] files);
}
