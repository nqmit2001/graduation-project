package com.nqm.graduation.file.service.impl;

import com.nqm.graduation.common.exception.ErrorException;
import com.nqm.graduation.file.dto.FileInfoResponse;
import com.nqm.graduation.file.entity.FileInfo;
import com.nqm.graduation.file.mapper.FileInforMapper;
import com.nqm.graduation.file.repository.FileStorageRepository;
import com.nqm.graduation.file.service.FileStorageService;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.FileNameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
@Slf4j
public class FileStorageSeviceImpl implements FileStorageService {

    @Value("${upload.file.path}")
    private String uploadFilePath;

    @Value("${minio.bucket}")
    private String minioBucket;
    @Value("${minio.endpoint}")
    private String minioEndpoint;

    @Value("${minio.public-endpoint}")
    private String minioPublicEndpoint;

    private final FileStorageRepository fileInfoRepository;

    private final MinioClient minioClient;

    private final FileInforMapper fileInforMapper;

    private Path root;

    public FileStorageSeviceImpl(FileStorageRepository fileInfoRepository,
                                 MinioClient minioClient,
                                 FileInforMapper fileInforMapper) {
        this.fileInfoRepository = fileInfoRepository;
        this.minioClient = minioClient;
        this.fileInforMapper = fileInforMapper;
    }

    @Override
    public FileInfoResponse findById(Long id){
        FileInfo entity = fileInfoRepository.findById(id).orElseThrow(() -> new ErrorException("Not found file id: " + id));
        return fileInforMapper.toResponse(entity);
    }

    @Override
    public List<FileInfoResponse> findByIds(List<Long> ids){
        List<FileInfo> entities = fileInfoRepository.findAllById(ids);
        return fileInforMapper.toResponse(entities);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public String uploadFile(MultipartFile file) {
        FileInfo newFile = saveFileLocally(file);
        fileInfoRepository.save(newFile);
        String uploadFileUrl = String.format("%s/%s/%s", minioPublicEndpoint, minioBucket, newFile.getFileName());
        try {
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket(minioBucket)
                            .object(newFile.getFileName())
                            .filename(newFile.getUrl())
                            .build());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            deleteAll();
        }
        return uploadFileUrl;
    }

    @Override
    public List<String> uploadMultipleFiles(MultipartFile[] files) {
        List<String> uploadUrls = new ArrayList<>();
        for (MultipartFile file: files) {
            String url = this.uploadFile(file);
            uploadUrls.add(url);
        }
        return uploadUrls;
    }

    public void deleteAll() {
        this.root = Paths.get(uploadFilePath);
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    private FileInfo saveFileLocally(MultipartFile file) {
        String filename = UUID.randomUUID() + "." + FileNameUtils.getExtension(file.getOriginalFilename());;
        try {
            Path uploadPath = Paths.get(uploadFilePath);
            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }
            this.root = Paths.get(uploadFilePath);
            Files.copy(file.getInputStream(), this.root.resolve(Objects.requireNonNull(filename)));
        } catch (Exception e) {
            if (e instanceof FileAlreadyExistsException) {
                saveFileLocally(file);
            }
            throw new ErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
        }
        return FileInfo
                .builder()
                .fileName(filename)
                .originalFilename(file.getOriginalFilename())
                .fileSize(file.getSize())
                .url(this.uploadFilePath + "/" + filename)
                .mimeType(file.getContentType())
                .build();
    }
}
