package com.nqm.graduation.iam.entity;


import com.nqm.graduation.common.auditting.AuditableEntity;
import com.nqm.graduation.common.utils.Enums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "users")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Users extends AuditableEntity {
    @Id
    @Column(name = "id", nullable = false, length = 20)
    private String id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "fullname", nullable = false)
    private String fullname;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "numberphone")
    private String numberphone;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Enums.Gender gender = Enums.Gender.OTHER;

    @Column(name = "is_deleted")
    private Boolean isDeleted = Boolean.FALSE;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Enums.UserStatus status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users user = (Users) o;
        return Objects.equals(id, user.id) && id != null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
