package com.nqm.graduation.common.auditting;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;

@MappedSuperclass
public abstract class AuditableEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @CreatedDate
    @Column(
            name = "created_at",
            updatable = false
    )
    protected Instant createdAt = Instant.now();

    @LastModifiedDate
    @Column(
            name = "last_modified_at"
    )
    protected Instant lastModifiedAt = Instant.now();

//    @CreatedBy
//    @Column(name = "created_by", updatable = false)
//    protected String createdBy;
//
//    @LastModifiedBy
//    @Column(name = "last_modified_by")
//    protected String lastModifiedBy;

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Instant getLastModifiedAt() {
        return this.lastModifiedAt;
    }

    public void setCreatedAt(final Instant createdAt) {
        this.createdAt = createdAt;
    }

    public void setLastModifiedAt(final Instant lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

//    public String getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(String createdBy) {
//        this.createdBy = createdBy;
//    }
//
//    public String getLastModifiedBy() {
//        return lastModifiedBy;
//    }
//
//    public void setLastModifiedBy(String lastModifiedBy) {
//        this.lastModifiedBy = lastModifiedBy;
//    }
}
