package com.nqm.graduation.common.minio;

import com.nqm.graduation.common.exception.ErrorException;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Slf4j
@Configuration
public class MinioConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinioConfiguration.class);

    @Value("${minio.credential.username}")
    String minioUsername;
    @Value("${minio.credential.password}")
    String minioPassword;
    @Value("${minio.credential.access}")
    String minioAccessKey;
    @Value("${minio.credential.secret}")
    String minioSecretKey;
    @Value("${minio.bucket}")
    String minioBucket;
    @Value("${minio.endpoint}")
    String minioEndPoint;

    @Bean
    public MinioClient generateMinioClient() {
        try {
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint(minioEndPoint)
                            .credentials(minioAccessKey, minioSecretKey)
                            .build();
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioBucket).build());
            LOGGER.info("Connect Minio Successfully");
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioBucket).build());
                LOGGER.info("Create New Bucket {} Successfully", minioBucket);
            }
            return minioClient;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

    }
}
