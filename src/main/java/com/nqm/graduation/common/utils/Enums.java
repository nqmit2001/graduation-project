package com.nqm.graduation.common.utils;

public class Enums {
    public enum Gender{
        MALE,
        FEMALE,
        OTHER
    }

    public enum UserStatus{
        ACTIVE,
        INACTIVE;
    }
}
